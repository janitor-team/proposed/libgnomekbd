Source: libgnomekbd
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper (>= 11),
               gnome-pkg-tools (>= 0.10),
               gobject-introspection (>= 0.9.12-4~),
               libatk1.0-dev (>= 1.32.0-2~),
               libgirepository1.0-dev (>= 0.9.12),
               libglib2.0-dev (>= 2.26),
               libgtk-3-dev (>= 3.0.0),
               libxklavier-dev (>= 5.2),
               pkg-config
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/gnome-team/libgnomekbd
Vcs-Git: https://salsa.debian.org/gnome-team/libgnomekbd.git

Package: libgnomekbd-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-gkbd-3.0 (= ${binary:Version}),
         libglib2.0-dev (>= 2.26),
         libgnomekbd8 (= ${binary:Version}),
         libgtk-3-dev (>= 3.0.0),
         libx11-dev,
         libxklavier-dev (>= 5.2),
         ${misc:Depends},
         ${shlibs:Depends}
Description: GNOME library to manage keyboard configuration - development files
 libgnomekbd offers an API to manage the keyboard in GNOME applications.
 .
 libgnomekbdui offers an API to display a graphical user interface for
 libgnomekbd operations.
 .
 This package contains the development files.

Package: libgnomekbd8
Section: libs
Architecture: any
Multi-Arch: same
Depends: iso-codes,
         libgnomekbd-common (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: GNOME library to manage keyboard configuration - shared library
 libgnomekbd offers an API to manage the keyboard in GNOME applications.
 .
 libgnomekbdui offers an API to display a graphical user interface for
 libgnomekbd operations.
 .
 This package contains the shared library.

Package: libgnomekbd-common
Section: libs
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: GNOME library to manage keyboard configuration - common files
 libgnomekbd offers an API to manage the keyboard in GNOME applications.
 .
 libgnomekbdui offers an API to display a graphical user interface for
 libgnomekbd operations.
 .
 This package contains files common to the various libgnomekbd packages.

Package: gkbd-capplet
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: GNOME control center tools for libgnomekbd
 libgnomekbd offers an API to manage the keyboard in GNOME applications.
 .
 This package contains a helper for the GNOME control center to
 configure and display keyboard mappings.

Package: gir1.2-gkbd-3.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Description: GObject introspection data for the GnomeKbd library
 libgnomekbd offers an API to manage the keyboard in GNOME applications.
 .
 libgnomekbdui offers an API to display a graphical user interface for
 libgnomekbd operations.
 .
 This package contains introspection data for the GnomeKbd library.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.
